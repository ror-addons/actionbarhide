ActionBarHide = {}

ActionBarHide.bar =
{
	[1] = { inc_alpha = 1, ooc_alpha = 0, mod_bar = true },
	[2] = { inc_alpha = 1, ooc_alpha = 0, mod_bar = true },
	[3] = { inc_alpha = 1, ooc_alpha = 0, mod_bar = true },
	[4] = { inc_alpha = 1, ooc_alpha = 0, mod_bar = true },
	[5] = { inc_alpha = 1, ooc_alpha = 0, mod_bar = true },
}
ActionBarHide.Settings = {}
ActionBarHide.Settings.Throttle = true
ActionBarHide.Settings.FadeInDelay = 0
ActionBarHide.Settings.FadeOutDelay = 1
ActionBarHide.Settings.FadeInTime = 0.9
ActionBarHide.Settings.FadeOutTime = 0.5
ActionBarHide.Settings.OutOfCombatMouseOverAlpha = 1

ActionBarHide.hidden = false
ActionBarHide.MouseIn = false

local systemTime = 0.0
local lastUpdate = 0.0

function ActionBarHide.Initialize()
	RegisterEventHandler(SystemData.Events.LOADING_END, "ActionBarHide.OnLoad")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "ActionBarHide.OnLoad")
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "ActionBarHide.Combat")
	-- ActionBarHide.Settings.FadeInDelay = 0
	-- ActionBarHide.Settings.FadeOutDelay = 1
	-- ActionBarHide.Settings.FadeInTime = 0.9
	-- ActionBarHide.Settings.FadeOutTime = 0.5

	--[[ Grab the old mouse handler and add our alpha
	--change function ]]
	old_mouse_over = ActionButton.OnMouseOver
	ActionButton.OnMouseOver = function(...)
		old_mouse_over(...)
		ActionBarHide.MouseIn = true
		ActionBarHide.ChangeAlpha()
	end

	--[[ Grab the old mouse handler and add our alpha
	--change function ]]
	old_mouse_over_end = ActionButton.OnMouseOverEnd
	ActionButton.OnMouseOverEnd = function(...)
		old_mouse_over_end(...)
		ActionBarHide.MouseIn = false
		ActionBarHide.ChangeAlpha()
	end

	-- 21-Jul-17 The chunk below is not needed when  using WindowStartAlphaAnimation to hide bars.
	-- It's probably needed when using WindowSetAlpha, since i got some ugly outcomes when using it without the chunk
	-- Although it may produce some ugliness together too (e.g. a delay betweed (dis)apperance of labels and everything else)

	--[[ Grab the Update function.  This is called
	--each frame for each button on each action bar. ]]
	-- old_button_update = ActionButton.Update
	-- ActionButton.Update = function(self, timeElapsed, updateCooldown, ui, pr, cr)
	-- 	if (nil == self.time) then
	-- 		self.time = 0.0
	-- 		self.text_shown = true
	-- 		self.pr = 0
	-- 	end
	-- 	self.time = self.time + timeElapsed
	-- 	if (false == ActionBarHide.Settings.Throttle) then
	-- 		old_button_update(self, timeElapsed, updateCooldown, ui, pr, cr)
	-- 	end
	-- 	if (self.time >= 0.25) then
	-- 		if (true == ActionBarHide.Settings.Throttle) then
	-- 			old_button_update(self, self.time, true, ui, self.pr, cr)
	-- 			self.pr = cr
	-- 		end

	-- 		if (true == self.text_shown)and(true == ActionBarHide.hidden) then
	-- 			LabelSetText(self:GetName().."ActionText", L"")
	-- 			self.text_shown = false
	-- 		elseif (false == self.text_shown)and(false == ActionBarHide.hidden) then
	-- 			self:UpdateKeyBindingText()
	-- 			self.text_shown = true
	-- 		end

	-- 		self.time = 0.0
	-- 	end
	-- end

end

--[[ On Load Event handler ]]
local registered = false
function ActionBarHide.OnLoad()
	EA_ChatWindow.Print(L"ActionBarHide Loaded")

	if ((nil ~= LibSlash)and(false == registered)) then
		LibSlash.RegisterSlashCmd("abh", function() ActionBarHide.OptionsWindow() end)
		registered = true
	end

	WindowSetShowing("ActionBarLockToggler", false)

	ActionBarHide.ChangeAlpha()
end

--[[
--	Called when Combat State Changes
--]]
ActionBarHide.last_state = false
function ActionBarHide.Combat()
	if ((true == GameData.Player.inCombat)and(false == ActionBarHide.last_state)) then
		ActionBarHide.ChangeAlpha()
	elseif (true == ActionBarHide.last_state) then
		ActionBarHide.ChangeAlpha()
	end
end



--[[ 
--	Change the alpha depending on current combat state
--	and the state the current bar is in
--]]
function ActionBarHide.ChangeAlpha()
	for k=1,5 do
		if (true == ActionBarHide.bar[k].mod_bar) then
			if (ActionBarHide.MouseIn) and not (GameData.Player.inCombat) then
				local alphastart = WindowGetAlpha("EA_ActionBar"..k)
				local targetalpha = ActionBarHide.Settings.OutOfCombatMouseOverAlpha 
				ActionBarHide.hidden = false
				-- if ActionBarHide.Settings.FadeInTime > 0 then
					WindowStartAlphaAnimation("EA_ActionBar"..k, Window.AnimationType.SINGLE_NO_RESET, alphastart, targetalpha, 
						ActionBarHide.Settings.FadeInTime, false, ActionBarHide.Settings.FadeInDelay, 0 )
				-- else
				-- 	WindowSetAlpha("EA_ActionBar"..k, ActionBarHide.bar[k].inc_alpha) -- this thing may not hide labels
				-- end
			elseif
				 (true == GameData.Player.inCombat) 
			then
				local alphastart = WindowGetAlpha("EA_ActionBar"..k)
				local targetalpha = ActionBarHide.bar[k].inc_alpha 
				ActionBarHide.hidden = false
				-- if ActionBarHide.Settings.FadeInTime > 0 then
					WindowStartAlphaAnimation("EA_ActionBar"..k, Window.AnimationType.SINGLE_NO_RESET, alphastart, targetalpha, 
						ActionBarHide.Settings.FadeInTime, false, ActionBarHide.Settings.FadeInDelay, 0 )
				-- else
				-- 	WindowSetAlpha("EA_ActionBar"..k, ActionBarHide.bar[k].inc_alpha) -- this thing may not hide labels
				-- end
			else
				local alphastart = WindowGetAlpha("EA_ActionBar"..k)
				local targetalpha = ActionBarHide.bar[k].ooc_alpha
				ActionBarHide.hidden = true
				-- if ActionBarHide.Settings.FadeOutTime > 0 then
					WindowStartAlphaAnimation("EA_ActionBar"..k, Window.AnimationType.SINGLE_NO_RESET, alphastart, targetalpha, 
						ActionBarHide.Settings.FadeOutTime, false, ActionBarHide.Settings.FadeOutDelay, 0 )
				-- else
				-- 	WindowSetAlpha("EA_ActionBar"..k, ActionBarHide.bar[k].ooc_alpha)
				-- end
			end
			-- if targetalpha > alphastart then
			-- 	WindowStartAlphaAnimation("EA_ActionBar"..k, Window.AnimationType.SINGLE_NO_RESET, alphastart, targetalpha, 
			-- 		ActionBarHide.Settings.FadeInTime, false, ActionBarHide.Settings.FadeInDelay, 0 )
			-- else
			-- 	WindowStartAlphaAnimation("EA_ActionBar"..k, Window.AnimationType.SINGLE_NO_RESET, alphastart, targetalpha, 
			-- 		ActionBarHide.Settings.FadeOutTime, false, ActionBarHide.Settings.FadeOutDelay, 0 )
			-- end
		end
	end
	ActionBarHide.last_state = GameData.Player.inCombat
end


--[[ 
--	GUI Config 
--]]
local LibGUI = LibStub("LibGUI")
function ActionBarHide.OptionsWindow()
	local w = {}

	w = LibGUI("Blackframe")
	w:MakeMovable()
	w:Resize(350, 450)
	w:AnchorTo("Root", "topright", "topright", 0, 0)

		-- Add a close button
	w.CloseButton = w("Closebutton")
	w.CloseButton.OnLButtonUp =
	function()
		w:Destroy()
	end

	-- Window title
	w.Title = w("Label")
	w.Title:Resize(250)
	w.Title:AnchorTo(w, "top", "top", 0, 10)
	w.Title:Font("font_clear_large_bold")
	w.Title:SetText(L"ABH Config")
	w.Title:Align("center")
	w.Title:IgnoreInput()

	-- in combat alpha
	w.l1 = w("Label")
	w.l1:Resize(260)
	w.l1:Position(25,45)
	w.l1:Font("font_clear_medium")
	w.l1:SetText(L"In Combat Alpha:")
	w.l1:Align("left")

	w.s1 = w("Slider")
	w.s1:AnchorTo(w.l1, "bottomleft", "topleft", 0, 0)
	w.s1:SetRange(0.0, 1.0)
	w.s1:SetValue(ActionBarHide.bar[1].inc_alpha)

	w.la1 = w("Label")
	w.la1:Resize(50)
	w.la1:AnchorTo(w.s1, "right", "left", 0, 0)
	w.la1:Font("font_clear_medium")
	w.la1:SetText(towstring(tostring(ActionBarHide.bar[1].inc_alpha)))
	w.la1:Align("left")

	local fun = 
		function()
			local val = w.s1:GetValue()
			-- val = floor(val * 100)
			-- val = val / 100
			ActionBarHide.bar[1].inc_alpha = val
			ActionBarHide.bar[2].inc_alpha = val
			ActionBarHide.bar[3].inc_alpha = val
			ActionBarHide.bar[4].inc_alpha = val
			ActionBarHide.bar[5].inc_alpha = val
			w.la1:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s1.OnLButtonUp = fun
	w.s1.OnMouseOverEnd =fun


	-- out of combat alpha
	w.l2 = w("Label")
	w.l2:Resize(260)
	--w.l2:Position(250,35)
	w.l2:AnchorTo(w.s1, "bottomleft", "topleft", 0, 0)
	w.l2:Font("font_clear_medium")
	w.l2:SetText(L"Out Of Combat Alpha:")
	w.l2:Align("left")

	w.s2 = w("Slider")
	w.s2:AnchorTo(w.l2, "bottomleft", "topleft", 0, 0)
	w.s2:SetRange(0.0, 1.0)
	w.s2:SetValue(ActionBarHide.bar[1].ooc_alpha)

	w.la2 = w("Label")
	w.la2:Resize(50)
	w.la2:AnchorTo(w.s2, "right", "left", 0, 0)
	w.la2:Font("font_clear_medium")
	w.la2:SetText(towstring(tostring(ActionBarHide.bar[1].ooc_alpha)))
	w.la2:Align("left")

	local fun = 
		function()
			local val = w.s2:GetValue()
			--val = ceil(val * 100) / 100
			ActionBarHide.bar[1].ooc_alpha = val
			ActionBarHide.bar[2].ooc_alpha = val
			ActionBarHide.bar[3].ooc_alpha = val
			ActionBarHide.bar[4].ooc_alpha = val
			ActionBarHide.bar[5].ooc_alpha = val
			w.la2:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s2.OnLButtonUp = fun
	w.s2.OnMouseOverEnd = fun

	-- fade out time
	w.l3 = w("Label")
	w.l3:Resize(260)
	w.l3:AnchorTo(w.s2, "bottomleft", "topleft", 0, 0)
	--w.l3:Position(250,35)
	w.l3:Font("font_clear_medium")
	w.l3:SetText(L"Fade Out Time:")
	w.l3:Align("left")

	w.s3 = w("Slider")
	w.s3:AnchorTo(w.l3, "bottomleft", "topleft", 0, 0)
	w.s3:SetRange(0.0, 2.0)
	w.s3:SetValue(ActionBarHide.Settings.FadeOutTime)

	w.la3 = w("Label")
	w.la3:Resize(50)
	w.la3:AnchorTo(w.s3, "right", "left", 0, 0)
	w.la3:Font("font_clear_medium")
	w.la3:SetText(towstring(tostring(ActionBarHide.Settings.FadeOutTime)))
	w.la3:Align("left")

	local fun = 
		function()
			local val = w.s3:GetValue()
			if val < 0.05 then
				val = 0.05
			end
			ActionBarHide.Settings.FadeOutTime = val
			w.la3:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s3.OnLButtonUp = fun
	w.s3.OnMouseOverEnd = fun

	-- fade in time
	w.l4 = w("Label")
	w.l4:Resize(260)
	w.l4:AnchorTo(w.s3, "bottomleft", "topleft", 0, 0)
	w.l4:Font("font_clear_medium")
	w.l4:SetText(L"Fade In Time:")
	w.l4:Align("left")

	w.s4 = w("Slider")
	w.s4:AnchorTo(w.l4, "bottomleft", "topleft", 0, 0)
	w.s4:SetRange(0.0, 2.0)
	w.s4:SetValue(ActionBarHide.Settings.FadeInTime)

	w.la4 = w("Label")
	w.la4:Resize(50)
	w.la4:AnchorTo(w.s4, "right", "left", 0, 0)
	w.la4:Font("font_clear_medium")
	w.la4:SetText(towstring(tostring(ActionBarHide.Settings.FadeInTime)))
	w.la4:Align("left")

	local fun = 
		function()
			local val = w.s4:GetValue()
			if val < 0.05 then
				val = 0.05
			end
			ActionBarHide.Settings.FadeInTime = val
			w.la4:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s4.OnLButtonUp = fun
	w.s4.OnMouseOverEnd = fun

	-- fade out delay
	w.l5 = w("Label")
	w.l5:Resize(260)
	w.l5:AnchorTo(w.s4, "bottomleft", "topleft", 0, 0)
	w.l5:Font("font_clear_medium")
	w.l5:SetText(L"Fade Out Delay:")
	w.l5:Align("left")

	w.s5 = w("Slider")
	w.s5:AnchorTo(w.l5, "bottomleft", "topleft", 0, 0)
	w.s5:SetRange(0.0, 4.0)
	w.s5:SetValue(ActionBarHide.Settings.FadeOutDelay)

	w.la5 = w("Label")
	w.la5:Resize(50)
	w.la5:AnchorTo(w.s5, "right", "left", 0, 0)
	w.la5:Font("font_clear_medium")
	w.la5:SetText(towstring(tostring(ActionBarHide.Settings.FadeOutDelay)))
	w.la5:Align("left")

	local fun = 
		function()
			local val = w.s5:GetValue()
			ActionBarHide.Settings.FadeOutDelay = val
			w.la5:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s5.OnLButtonUp = fun
	w.s5.OnMouseOverEnd = fun

	-- fade out delay
	w.l6 = w("Label")
	w.l6:Resize(300)
	w.l6:AnchorTo(w.s5, "bottomleft", "topleft", 0, 0)
	w.l6:Font("font_clear_medium")
	w.l6:SetText(L"Out Of Combat Mouseover Alpha:")
	w.l6:Align("left")

	w.s6 = w("Slider")
	w.s6:AnchorTo(w.l6, "bottomleft", "topleft", 0, 0)
	w.s6:SetRange(0.0, 1.0)
	w.s6:SetValue(ActionBarHide.Settings.OutOfCombatMouseOverAlpha)

	w.la6 = w("Label")
	w.la6:Resize(50)
	w.la6:AnchorTo(w.s6, "right", "left", 0, 0)
	w.la6:Font("font_clear_medium")
	w.la6:SetText(towstring(tostring(ActionBarHide.Settings.OutOfCombatMouseOverAlpha)))
	w.la6:Align("left")

	local fun = 
		function()
			local val = w.s6:GetValue()
			ActionBarHide.Settings.OutOfCombatMouseOverAlpha = val
			w.la6:SetText(towstring(tostring(val)))
			ActionBarHide.ChangeAlpha()
		end
	w.s6.OnLButtonUp = fun
	w.s6.OnMouseOverEnd = fun
	w.s6.OnMouseOver = 
		function()
			local val = w.s6:GetValue()
			ActionBarHide.Settings.OutOfCombatMouseOverAlpha = val
			w.la6:SetText(towstring(tostring(val)))
			--ActionBarHide.ChangeAlpha()
		end
	-- throttle checkbox
	-- w.l3 = w("Label")
	-- w.l3:Resize(200)
	-- w.l3:Position(25,100)
	-- w.l3:Font("font_clear_medium")
	-- w.l3:SetText(L"Useless checkbox") --w.l3:SetText(L"Throttle:")
	-- w.l3:Align("left")

	-- w.k1 = w("Checkbox")
	-- w.k1:AnchorTo(w.l3, "right", "left", 0, 0)
	-- w.k1.OnLButtonUp =
	-- 	function()
	-- 		ActionBarHide.Settings.Throttle = w.k1:GetValue()
	-- 	end
	-- if (true == ActionBarHide.Settings.Throttle) then
	-- 	w.k1:Check()
	-- end

	w:Show() 
end

--[[
	Would be nice to make an option to display glowing buttons - shams/ams/borks/sms can be looking at those
--]]