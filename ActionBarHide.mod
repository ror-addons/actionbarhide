<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<UiMod name="ActionBarHide" version="1.1" date="22-Jul-17 4:08:31" >

	<Author name="Metaphaze, 2017 update by anon" email="metaphaze@gmail.com" />
	<Description text="Hides Action bars when out of combat. Config: /abh" />

	<Dependencies>
		<Dependency name="EASystem_LayoutEditor" />
		<Dependency name="EASystem_WindowUtils" />
	</Dependencies>

	<Files>
		<File name="LibStub.lua" />
		<File name="LibGui.lua" />
		<File name="ActionBarHide.lua" />
	</Files>

	<SavedVariables>
		<SavedVariable name="ActionBarHide.bar" />
		<SavedVariable name="ActionBarHide.Settings" />
	</SavedVariables>

	<OnInitialize>
		<CallFunction name="ActionBarHide.Initialize" />
	</OnInitialize>

</UiMod>
</ModuleFile>
