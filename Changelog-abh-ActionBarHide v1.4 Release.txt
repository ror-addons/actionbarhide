------------------------------------------------------------------------
r12 | metaphaze | 2008-11-22 16:15:09 +0000 (Sat, 22 Nov 2008) | 2 lines
Changed paths:
   A /tags/ActionBarHide v1.4 Release (from /trunk:11)

Typos are fur wusses
Tagging as ActionBarHide v1.4 Release
------------------------------------------------------------------------
r11 | metaphaze | 2008-11-22 00:34:03 +0000 (Sat, 22 Nov 2008) | 3 lines
Changed paths:
   M /trunk/ActionBarHide.lua

- Typos are for wusses!


------------------------------------------------------------------------
r9 | metaphaze | 2008-11-21 06:54:26 +0000 (Fri, 21 Nov 2008) | 4 lines
Changed paths:
   M /trunk/ActionBarHide.lua

Fix for that glowy colour stuff Shaman/Archmage
get


------------------------------------------------------------------------
r8 | metaphaze | 2008-11-21 06:36:06 +0000 (Fri, 21 Nov 2008) | 3 lines
Changed paths:
   M /trunk/ActionBarHide.lua

- Registered in the wrong place, whoops


------------------------------------------------------------------------
r6 | metaphaze | 2008-11-21 06:20:28 +0000 (Fri, 21 Nov 2008) | 5 lines
Changed paths:
   M /trunk/ActionBarHide.lua
   M /trunk/ActionBarHide.mod
   A /trunk/LibGUI.lua
   A /trunk/LibStub.lua

- Add a GUI
- Allow different in and out of combat alphas
- Add button redraw throttle


------------------------------------------------------------------------
r4 | metaphaze | 2008-11-21 01:45:16 +0000 (Fri, 21 Nov 2008) | 4 lines
Changed paths:
   M /trunk/ActionBarHide.lua

Fix for when you mouse over your bar while
in combat


------------------------------------------------------------------------
r2 | metaphaze | 2008-11-21 00:36:34 +0000 (Fri, 21 Nov 2008) | 3 lines
Changed paths:
   A /trunk/ActionBarHide.lua
   A /trunk/ActionBarHide.mod

Initial Commit of ActionBarHide


------------------------------------------------------------------------
r1 | root | 2008-11-21 00:19:26 +0000 (Fri, 21 Nov 2008) | 1 line
Changed paths:
   A /branches
   A /tags
   A /trunk

"abh/mainline: Initial Import"
------------------------------------------------------------------------
